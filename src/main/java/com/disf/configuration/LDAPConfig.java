package com.disf.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.core.support.LdapContextSource;

import javax.annotation.Resource;

/**
 * Created by denis on 6/14/16.
 */

@Configuration
public class LDAPConfig {

    @Resource
    Environment env;

    @Bean
    public LdapContextSource ldapContext() {
        LdapContextSource ldapContextSource = new LdapContextSource();
        ldapContextSource.setUrl(env.getProperty("ldap.url"));
        ldapContextSource.setUserDn(env.getProperty("ldap.userDN"));
        ldapContextSource.setBase(env.getProperty("ldap.base"));
        ldapContextSource.setPassword(env.getProperty("ldap.password"));
        return ldapContextSource;
    }

    @Bean
    public LdapTemplate ldapTemplate() {
        return new LdapTemplate(ldapContext());
    }

}
