package com.disf.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.ldap.core.support.LdapContextSource;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.authentication.configurers.GlobalAuthenticationConfigurerAdapter;

/**
 * Created by denis on 6/15/16.
 */
@Configuration
public class AuthConfig extends GlobalAuthenticationConfigurerAdapter {

    @Autowired
    public LdapContextSource ldapContextSource;

    @Override
    public void init(AuthenticationManagerBuilder authenticationManagerBuilder) throws Exception {
        authenticationManagerBuilder
                .ldapAuthentication()
                .userDnPatterns("uid={0}")
                .contextSource(ldapContextSource);
    }
}