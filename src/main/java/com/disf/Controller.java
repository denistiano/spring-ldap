package com.disf;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ldap.core.AttributesMapper;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by denis on 6/14/16.
 */

@RestController
public class Controller {

    @Autowired
    LdapTemplate ldapTemplate;

    @RequestMapping("/home")
    public String home() {
        String user = SecurityContextHolder.getContext().getAuthentication().getName();
        String username = ldapTemplate.search("uid=" + user, "(objectClass=*)", (AttributesMapper<Object>) attributes -> attributes.get("cn").get()).toString();
        return "Hello, " + username.replace("[", "").replace("]", "") + "!";
    }

}
